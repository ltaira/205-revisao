package br.com.itau.hardinvest.service;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.hardinvest.models.Cliente;
import br.com.itau.hardinvest.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	ClienteRepository clienteRepository;

	public List<Cliente> listarClientes() {
		List<Cliente> listaCliente = new ArrayList<>();
		Iterable<Cliente> iteravel = clienteRepository.findAll();
		iteravel.forEach(listaCliente::add);
		return listaCliente;
	}

	public Cliente criarCliente(Cliente cliente) {
		if(!isCPF(cliente.getCpf().replaceAll("[-.]", "")))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "CPF " + cliente.getCpf() +" invalido!");

		cliente.setCpf(FormatarCPf(cliente.getCpf()));
		return clienteRepository.save(cliente);
	}

	private String FormatarCPf(String cpf) {
		if(!cpf.contains(".") || !cpf.contains("-"))
			return (cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." +
					cpf.substring(6, 9) + "-" + cpf.substring(9, 11));
		
		return cpf;
	}

	public Cliente atualizarClienteCasoExistaNaBase(Cliente cliente, Long idCliente) {
		Cliente clienteBaseDados = procurarClienteBaseDeDados(idCliente);
		clienteBaseDados.setNome(cliente.getNome());
		return clienteRepository.save(clienteBaseDados);
	}
	
	public void deletarClienteCasoExistaBase(Long idCliente) {
		Cliente clienteBaseDados = procurarClienteBaseDeDados(idCliente);
		clienteRepository.delete(clienteBaseDados);
	}

	private Cliente procurarClienteBaseDeDados(Long idCliente) {
		Optional<Cliente> cliente = clienteRepository.findById(idCliente);

		if (!cliente.isPresent())
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);

		return cliente.get();
	}
	
	public static boolean isCPF(String CPF) {
        
        if (CPF.equals("00000000000") ||
            CPF.equals("11111111111") ||
            CPF.equals("22222222222") || CPF.equals("33333333333") ||
            CPF.equals("44444444444") || CPF.equals("55555555555") ||
            CPF.equals("66666666666") || CPF.equals("77777777777") ||
            CPF.equals("88888888888") || CPF.equals("99999999999") ||
            (CPF.length() != 11))
            return(false);
          
        char dig10, dig11;
        int sm, i, r, num, peso;
          
        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
        // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i=0; i<9; i++) {              
        // converte o i-esimo caractere do CPF em um numero:
        // por exemplo, transforma o caractere '0' no inteiro 0         
        // (48 eh a posicao de '0' na tabela ASCII)         
            num = (int)(CPF.charAt(i) - 48); 
            sm = sm + (num * peso);
            peso = peso - 1;
            }
          
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); // converte no respectivo caractere numerico
          
        // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for(i=0; i<10; i++) {
            num = (int)(CPF.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
            }
          
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                 dig11 = '0';
            else dig11 = (char)(r + 48);
          
        // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                 return(true);
            else return(false);
                } catch (InputMismatchException erro) {
                return(false);
            }
        }
}
