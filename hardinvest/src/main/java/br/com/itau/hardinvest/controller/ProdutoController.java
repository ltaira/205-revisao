package br.com.itau.hardinvest.controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.hardinvest.models.ProdutoModels;
import br.com.itau.hardinvest.service.ProdutoService;

@RestController
@RequestMapping(value = "/produto")
public class ProdutoController {
	
	@Autowired
	ProdutoService produtoService;
	
	@GetMapping
	public Iterable<ProdutoModels> obterProdutos(){
		return produtoService.obterProdutos();
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.OK)
	public ProdutoModels criarProduto(@RequestBody ProdutoModels produto) {
		return produtoService.criarProduto(produto);
	}
	
	@PutMapping ("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public ProdutoModels alteraProduto(@PathVariable Long id, @RequestBody ProdutoModels produto) {
		return produtoService.alteraProduto(id, produto);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void excluirProduto(@PathVariable Long id) {
		produtoService.excluirProduto(id);
	}
	
}
