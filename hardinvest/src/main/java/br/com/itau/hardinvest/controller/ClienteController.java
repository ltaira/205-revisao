package br.com.itau.hardinvest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.hardinvest.models.Cliente;
import br.com.itau.hardinvest.service.ClienteService;

@RestController
public class ClienteController {
	
	@Autowired
	ClienteService clienteService;

	@GetMapping("/cliente")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Cliente> listarClientes() {
		return clienteService.listarClientes();
	}
	
	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.OK)
	public Cliente criarCliente(@RequestBody Cliente cliente) {
		return clienteService.criarCliente(cliente);
	}
	
	@PutMapping("/cliente/{idCliente}")
	@ResponseStatus(code = HttpStatus.OK)
	public Cliente atualizarCliente(@RequestBody Cliente cliente, @PathVariable Long idCliente) {
		return clienteService.atualizarClienteCasoExistaNaBase(cliente, idCliente);
	}
	
	@DeleteMapping("/cliente/{idCliente}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deletarCliente(@PathVariable Long idCliente) {
		clienteService.deletarClienteCasoExistaBase(idCliente);
	}
}
