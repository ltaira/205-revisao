package br.com.itau.hardinvest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.hardinvest.models.ProdutoModels;
import br.com.itau.hardinvest.repository.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;

	public Iterable<ProdutoModels> obterProdutos(){
		return produtoRepository.findAll();
	}
	
	public ProdutoModels obterProdutos(Long id){
		Optional<ProdutoModels> optional = 	produtoRepository.findById(id);	

		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Produto não encontrado");
		}
		
		return optional.get();

	}
	
	public ProdutoModels criarProduto(ProdutoModels produto) {
		return produtoRepository.save(produto);
	}
	
	public ProdutoModels alteraProduto(Long id, ProdutoModels produto) {
		ProdutoModels produtoPersistido = obterProdutos(id);
		
		produtoPersistido.setNome(produto.getNome());	
		produtoPersistido.setRendimento(produto.getRendimento());
		
		return produtoRepository.save(produtoPersistido);
	}
	
	public void excluirProduto(Long id) {
		ProdutoModels produtoPersistido = obterProdutos(id);
		
		produtoRepository.delete(produtoPersistido);
	}
}
