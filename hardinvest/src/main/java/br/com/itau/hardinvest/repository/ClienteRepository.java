package br.com.itau.hardinvest.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.hardinvest.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{
	

}
