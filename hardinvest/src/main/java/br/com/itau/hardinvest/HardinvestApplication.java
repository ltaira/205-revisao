package br.com.itau.hardinvest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HardinvestApplication {

	public static void main(String[] args) {
		SpringApplication.run(HardinvestApplication.class, args);
	}

}
