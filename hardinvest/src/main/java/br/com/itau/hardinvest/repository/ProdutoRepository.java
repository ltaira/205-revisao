package br.com.itau.hardinvest.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.hardinvest.models.ProdutoModels;

public interface ProdutoRepository extends CrudRepository<ProdutoModels, Long> {

}
