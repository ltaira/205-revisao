package br.com.itau.hardinvest.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.hardinvest.models.ProdutoModels;
import br.com.itau.hardinvest.repository.ProdutoRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ProdutoService.class})
public class ProdutoServiceTest {
	
	@Autowired
	ProdutoService produtoService;
	
	@MockBean
	ProdutoRepository produtoRepository;
	
	@Test
	public void deveObterTodosOsProdutos() {
		List<ProdutoModels> produtos = new ArrayList<>();
		produtos.add(criarProduto(1L));

		when(produtoRepository.findAll()).thenReturn(produtos);
		
		Iterable<ProdutoModels> resultado = produtoService.obterProdutos();
		
		assertNotNull(resultado);
		assertEquals(produtos, resultado);
	}

	@Test
	public void deveObterOProdutoSelecionado() {
		Long id = 1L;
		ProdutoModels produto = criarProduto(1L);
		
		when(produtoRepository.findById(id)).thenReturn(Optional.of(produto));
		
		ProdutoModels resultado = produtoService.obterProdutos(id);
		
		assertNotNull(resultado);
		assertEquals(produto, resultado);
	}
	
	
	@Test(expected = ResponseStatusException.class)
	public void deveLancarExcecaoQuandoNaoEncontrarOProduto() {
		Long id = 1L;
		
		when(produtoRepository.findById(id)).thenReturn(Optional.empty());
		
		produtoService.obterProdutos(id);
	}

	@Test
	public void deveCriarUmProduto() {
		ProdutoModels produto = criarProduto(1L);
		
		when(produtoRepository.save(produto)).thenReturn(produto);
				
		ProdutoModels resultado = produtoService.criarProduto(produto);
		produtoService.criarProduto(produto);
		
		//verify(produtoRepository).save(produto);
		assertEquals(produto, resultado);
		
	}
	
	@Test
	public void deveEditarUmProduto() {
		Long id = 1L;
		Long novoId = 10L;
		String novoNome = "Poupança";
		Double novoRendimento = 2.0;
		
		ProdutoModels ProdutoPersistido = criarProduto(id);
		ProdutoModels ProdutoFront = criarProduto(novoId);
		ProdutoFront.setNome(novoNome);
		ProdutoFront.setRendimento(novoRendimento);
		
		when(produtoRepository.findById(id)).thenReturn(Optional.of(ProdutoPersistido));
		when(produtoRepository.save(ProdutoPersistido)).thenReturn(ProdutoPersistido);
		
		ProdutoModels resultado = produtoService.alteraProduto(id, ProdutoFront);
		
		//check
		assertNotNull(resultado);
		verify(produtoRepository).save(ProdutoPersistido);
		assertEquals(novoNome, resultado.getNome());
		assertEquals(novoRendimento, resultado.getRendimento());
		assertNotEquals(novoId, resultado.getId());
	}

	
	//@Test(expected = ResponseStatusException.class)
	@Test
	public void deveExcluirUmProduto() {
		//SETUP
		Long id = 1L;
		ProdutoModels produto = criarProduto(id);
		when(produtoRepository.findById(id)).thenReturn(Optional.of(produto));
		
		//ACTION
		produtoService.excluirProduto(id);
		
		
		//CHECK
		verify(produtoRepository).delete(produto);
	}

	private ProdutoModels criarProduto(Long id) {
		ProdutoModels produto = new ProdutoModels();
		produto.setId(id);
		produto.setNome("Fundo");
		produto.setRendimento(1.5);
			
		return produto;
	}

}
