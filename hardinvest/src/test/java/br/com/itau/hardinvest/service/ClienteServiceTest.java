package br.com.itau.hardinvest.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.hibernate.query.criteria.internal.expression.SearchedCaseExpression.WhenClause;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.hardinvest.models.Cliente;
import br.com.itau.hardinvest.repository.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClienteService.class)
public class ClienteServiceTest {
	@Autowired
	ClienteService clienteService;

	@MockBean
	ClienteRepository clienteRepository;

	@Test
	public void deveRetornarListaDeClientes() {
		// SETUP
		Cliente cliente = new Cliente();
		List<Cliente> listCliente = Lists.newArrayList(cliente);
		when(clienteRepository.findAll()).thenReturn(listCliente);

		// ACTION
		List<Cliente> listaRetornarda = new ArrayList<>();
		Iterable<Cliente> iteravel = clienteService.listarClientes();
		iteravel.forEach(listaRetornarda::add);

		// CHECK
		assertEquals(cliente, listaRetornarda.get(0));
	}

	@Test
	public void deveCriarCliente() {
		// SETUP
		Cliente cliente = new Cliente();
		cliente.setNome("Felipe");
		cliente.setCpf("39442277863");
		when(clienteRepository.save(cliente)).thenReturn(cliente);

		// ACTION
		Cliente clienteCriado = clienteService.criarCliente(cliente);

		// CHECK
		assertEquals(cliente, clienteCriado);
	}

	@Test(expected = ResponseStatusException.class)
	public void deveValidarCpfErrado11Dig() {
		// SETUP
		Cliente cliente = new Cliente();
		cliente.setNome("Felipe");
		cliente.setCpf("3944227783363");

		//Action
		clienteService.criarCliente(cliente);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveValidarCpfErradoNumerosIguais() {
		// SETUP
		Cliente cliente = new Cliente();
		cliente.setNome("Felipe");
		cliente.setCpf("00000000000");

		//Action
		clienteService.criarCliente(cliente);
	}
	
	@Test
	public void deveAtualizarCliente() {
		//SETUP
		Cliente clienteBaseDeDados = new Cliente();
		clienteBaseDeDados.setNome("Arlindo");
		Cliente clienteFront = new Cliente();
		clienteFront.setNome("Jonas");
		when(clienteRepository.findById(clienteBaseDeDados.getId())).thenReturn(Optional.of(clienteBaseDeDados));
		when(clienteRepository.save(clienteBaseDeDados)).thenReturn(clienteFront);
		
		//ACTION
		Cliente clienteAtualizado = clienteService.atualizarClienteCasoExistaNaBase(clienteFront, clienteBaseDeDados.getId());
		
		//CHECK
		assertEquals(clienteFront.getNome(), clienteAtualizado.getNome());
	}
	
	@Test
	public void deveDeletarCliente() {
		//SETUP
		Cliente clienteBaseDeDados = new Cliente();
		clienteBaseDeDados.setNome("Arlindo");
		clienteBaseDeDados.setId(1L);
		when(clienteRepository.findById(clienteBaseDeDados.getId())).thenReturn(Optional.of(clienteBaseDeDados));
		
		//ACTION
		clienteService.deletarClienteCasoExistaBase(clienteBaseDeDados.getId());
		
		//CHECK
		Mockito.verify(clienteRepository).delete(clienteBaseDeDados);
	}
	
	@Test(expected = ResponseStatusException.class)
	public void deveRetornarErroAoNaoEncontrarClienteParaDeletar() {
		//SETUP
		Cliente clienteBaseDeDados = new Cliente();
		clienteBaseDeDados.setNome("Arlindo");
		clienteBaseDeDados.setId(1L);
		when(clienteRepository.findById(clienteBaseDeDados.getId())).thenReturn(Optional.empty());
		
		//ACTION
		clienteService.deletarClienteCasoExistaBase(clienteBaseDeDados.getId());
		
	}
}
